#!/bin/bash
# Source function library.
. /etc/init.d/functions

start() {
        echo -n "Starting C3Demo: "
        if [ -z $(ps -ef | grep C3Demo | grep -v grep | awk '{print $3}') ];then
            /var/opt/C3Demo
        fi
}

stop() {
        echo -n "Shutting down C3Demo: "
            pid=$(ps -ef | grep C3Demo | grep -v grep | awk '{print $3}')
            kill -9 $pid
        return 0
}

case "$1" in
    start)
        start
        ;;
    stop)
        stop
        ;;
    status)
        pid=$(ps -ef | grep C3Demo | grep -v grep | awk '{print $3}')
        netstat -anp | grep LISTEN | grep $pid
        ;;
    restart)
        stop
        start
        ;;
    *)
        echo "Usage:  {start|stop|status|restart}"
        exit 1
        ;;
esac
exit $?