package main

import (
	"io/ioutil"
	"net/http/httptest"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestHandler(t *testing.T) {
	Convey("Given an http request with URI", t, func() {
		req := httptest.NewRequest("GET", "http://localhost/foo", nil)
		Convey("The URI value is returned in the response message.", func() {
			w := httptest.NewRecorder()
			handler(w, req)
			resp := w.Result()
			body, _ := ioutil.ReadAll(resp.Body)
			So(string(body), ShouldEqual, "URI: foo!")
		})
	})
}
