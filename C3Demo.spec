%define debug_package %{nil}
Name:           C3Demo
Version:        1
Release:        0
Summary:        A simple http service to return the URI that was passed.

Group:          WWT
BuildArch:      x86_64
License:        GPL
Source:         C3Demo.tgz

%description
A simple http service to return the URI that was passed.

%prep
%setup -q
%build
%install
install -m 0755 -d $RPM_BUILD_ROOT/var/opt/C3Demo
install -m 0755 C3Demo $RPM_BUILD_ROOT/var/opt/C3Demo/C3Demo
install -m 0755 init.sh $RPM_BUILD_ROOT/var/opt/C3Demo/init.sh

%clean

rm -rf $RPM_BUILD_ROOT

%files
/var/opt/C3Demo

%changelog